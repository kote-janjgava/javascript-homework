// Back to front string
// Create the function "backToFront" which gets a string and symbols count. And it should return a string. For example:

// backToFront('hello', 1); // ohelloo
// backToFront('abc', 3); // abcabcabc
// backToFront('world', 2); // ldworldld
// backToFront('world', 20); // world
function backToFront(str,n)
 {
  if (str.length>=n)
   {
   str_len = n;
 
  back = str.substring(str.length-n);
   return back + str + back;
 }
   else
     return str;
 }
console.log(backToFront('hello', 1)); // ohelloo
console.log(backToFront('abc', 3)); // abcabcabc
console.log(backToFront('world', 2)); // ldworldld
console.log(backToFront('world', 20)); // ldworldld



// Create the function "nearest" to find a value which is nearest to z from two given values (x and Y); For example:

// nearest(100, 22, 122); // 122;
// nearest(50, 22, 122); // 22;

function nearest(n,x, y) {
  if (x != y)
  {
  x1 = Math.abs(x - n);
  y1 = Math.abs(y - n);

  if (x1 < y1)
  {
    return x;
  }
  if (y1 < x1)
  {
    return y;
  }
  return 0;
  }
  else
    return false;
}

console.log(nearest(100, 22, 122)); // 122;
console.log(nearest(50, 22, 122)); // 22;



// Create the function "removeDuplicate" to remove all duplicated values from array; do not use a set. For example:

// removeDuplicate([1,2,3,2,3,1,1]); // [1,2,3]
// removeDuplicate(['a', 1, '2', 'b', 1, '2', 'b']); // ['a', 1, '2', 'b']


   function removeDuplicate(arr) {
        return arr.filter((item, 
            index) => arr.indexOf(item) === index);
    }
  
    console.log(removeDuplicate([1,2,3,2,3,1,1]));// [1,2,3]
    console.log(removeDuplicate(['a', 1, '2', 'b', 1, '2', 'b']));// ['a', 1, '2', 'b']
