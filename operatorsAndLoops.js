// Arrays difference
// Create a JavaScript function to find difference between two arrays. For example:

// arrayDiff([1, 2, 3], [1, 2, 3, 4, 5]) // result: [4, 5]
// arrayDiff(['a', 'b', 'c'], ['a', 'b']) // result: ['c']

function arrayDiff(a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}
console.log(arrayDiff([1, 2, 3], [1, 2, 3, 4, 5])); // result: [4, 5]
console.log(arrayDiff(['a', 'b', 'c'],['a', 'b'])); // result: ['c']

// Even Odd checker
// Create a JavaScript function which gets two numbers and iterates over them. Check if it is even or odd. For example:

// evenOrOdd(0, 3); // result: ['0 is even', '1 is odd', '2 is even', '3 is odd']
// evenOrOdd(2, 4); // result: ['2 is even', '3 is odd', '4 is even']
function evenOrOdd(x,y){
    
    for(let count =x; count<=y;count++){
 count%2==0? console.log(`${count} is even`):console.log(`${count} is odd`);
 ;
}
    
}
evenOrOdd(0, 3); // result: ['0 is even', '1 is odd', '2 is even', '3 is odd']
evenOrOdd(2, 4); // result: ['2 is even', '3 is odd', '4 is even']

// Create a JavaScript function to get a sum of all numbers in the given range.

// For example:

// rangeSum(5, 10); // result: 45
// rangeSum(0, 6); // result: 21 

function rangeSum(start, end) {
    return end * (end + 1) / 2 - start * (start + 1) / 2;
}
console.log(rangeSum(5, 10))
console.log(rangeSum(0, 6))