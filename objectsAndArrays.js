var names = [1, 2, 1, 1, 3]

function removeDuplicates(array) {
  const result = [];
  const map = {};

  for (let i = 0; i < array.length; i++) {
    if (map[array[i]]) {
      continue;
    } else {
      result.push(array[i]);
      map[array[i]] = true;
    }
  }
  return result;
}
console.log(removeDuplicates(names))
